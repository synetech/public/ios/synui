// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

enum PackageProduct: String, CaseIterable {

    case wholePackage = "SYNUI"

    var name: String {
        return rawValue
    }

    var product: Product {
        return .library(name: rawValue, targets: targets)
    }

    var targets: [String] {
        switch self {
        case .wholePackage:
            return PackageTarget.allCases.map { $0.name }
        }
    }
}

enum PackageDependency: String, CaseIterable {

    case rswift = "RswiftLibrary"

    var dependency: Target.Dependency {
        switch self {
        case .rswift:
            return .product(name: rawValue, package: "R.swift")
        }
    }

    static var packages: [Package.Dependency] {
        return [
            .package(url: "https://github.com/mac-cain13/R.swift", from: "7.0.1")
        ]
    }
}

enum PackageTarget: String, CaseIterable {

    case alert = "SYNAlert"
    case color = "SYNColor"
    case forms = "SYNForms"
    case image = "SYNImage"
    case modifiers = "SYNModifiers"
    case navigationBar = "SYNNavigationBar"
    case rswift = "SYNRSwift"
    case shadow = "SYNShadow"
    case views = "SYNViews"

    var name: String {
        return rawValue
    }

    var dependency: Target.Dependency {
        return Target.Dependency(stringLiteral: rawValue)
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .modifiers:
            return [PackageTarget.views.dependency]
        case .rswift:
            return [PackageDependency.rswift.dependency, PackageTarget.image.dependency]
        case .shadow:
            return [PackageTarget.modifiers.dependency]
        default:
            return []
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.target(name: $0.name, dependencies: $0.dependencies) }
    }
}

enum PackageTestTarget: String, CaseIterable {

    case color = "ColorTests"

    var name: String {
        return rawValue
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .color:
            return [PackageTarget.color.dependency]
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.testTarget(name: $0.name, dependencies: $0.dependencies) }
    }
}

let package = Package(
    name: "SYNUI",
    platforms: [.iOS(.v13), .macOS(.v10_15)],
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    products: PackageProduct.allCases.map { $0.product },
    // Dependencies declare other packages that this package depends on.
    dependencies: PackageDependency.packages,
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    targets: PackageTarget.targets + PackageTestTarget.targets
)
