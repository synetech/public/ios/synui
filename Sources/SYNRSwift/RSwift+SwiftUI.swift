//
//  RSwift+SwiftUI.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import RswiftResources
import SwiftUI
import SYNImage

// MARK: - SwiftUI font casting
public extension FontResource {

    func font(size: CGFloat) -> Font {
        Font.custom(name, size: size)
    }
}

// MARK: - SwiftUI color casting
public extension RswiftResources.ColorResource {

    var color: Color {
        Color(name)
    }
}

// MARK: - SwiftUI text casting
public extension StringResource {

    var localizedStringKey: LocalizedStringKey {
        LocalizedStringKey(String(key))
    }

    var text: Text {
        Text(localizedStringKey)
    }
}

// MARK: - StaticString to String
private extension String {

    init(_ staticString: StaticString) {
        self = staticString.withUTF8Buffer {
            String(decoding: $0, as: UTF8.self)
        }
    }
}

// MARK: - SwiftUI image casting
public extension RswiftResources.ImageResource {

    var image: Image {
        Image(name)
    }

    #if canImport(UIKit)
    var vectorImage: VectorImage {
        vectorImage()
    }

    func vectorImage(contentMode: UIView.ContentMode? = nil, tintColor: UIColor? = nil) -> VectorImage {
        VectorImage(name: name, contentMode: contentMode, tintColor: tintColor)
    }
    #endif
}

#endif
