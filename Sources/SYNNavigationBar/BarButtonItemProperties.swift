//
//  BarButtonItemProperties.swift
//  
//
//  Created by Petr Dušek on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

public enum BarButtonItemProperties {

    case title(String?)
    case target(AnyObject?)
    case action(Selector?)
    case color(UIColor?)
    case image(UIImage?)
}
#endif
