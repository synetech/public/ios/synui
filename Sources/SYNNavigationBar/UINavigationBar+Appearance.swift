//
//  UINavigationBar+Appearance.swift
//  BaseApp
//
//  Created by Štěpán Klouček on 07/08/2020.
//  Copyright © 2020 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UINavigationBar {

    // MARK: - Subtypes
    enum BackgroundType {

        case transparent
        case semiTransparent(color: UIColor?, alpha: CGFloat, includeShadow: Bool = true)
        case color(color: UIColor?)
        case image(image: UIImage?)
    }

    // MARK: - Interactions
    func setAppearance(tintColor: UIColor? = nil, font: UIFont? = nil, largeFont: UIFont? = nil,
                       background: BackgroundType? = nil) {
        if let tintColor = tintColor {
            self.tintColor = tintColor
        }
        if #available(iOS 13.0, *) {
            setAppearanceForiOS13AndHigher(font: font, largeFont: largeFont,
                                           tintColor: tintColor, background: background)
        } else {
            setTitleAppearanceForiOS12AndLower(tintColor: tintColor, font: font, largeFont: largeFont)
            setBackgroundForiOS12AndLower(background: background)
        }
    }
}

// MARK: - Appearance for iOS 13+
@available(iOS 13.0, *)
private extension UINavigationBar {

    func setAppearanceForiOS13AndHigher(font: UIFont?, largeFont: UIFont?, tintColor: UIColor?,
                                        background: BackgroundType?) {
        let appearance = UINavigationBarAppearance()

        let regularTextAttributes = getTextAttributes(tintColor: tintColor, font: font)
        appearance.titleTextAttributes = regularTextAttributes
        appearance.buttonAppearance.normal.titleTextAttributes = regularTextAttributes
        appearance.buttonAppearance.focused.titleTextAttributes = regularTextAttributes
        appearance.buttonAppearance.highlighted.titleTextAttributes = regularTextAttributes
        appearance.largeTitleTextAttributes =
            getTextAttributes(tintColor: tintColor, font: largeFont ?? font?.withSize(17))

        switch background {
        case .transparent:
            appearance.configureWithTransparentBackground()
        case .semiTransparent(let color, let alpha, let includeShadow):
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = color?.withAlphaComponent(alpha)
            if !includeShadow {
                appearance.shadowImage = UIImage()
                appearance.shadowColor = .clear
            }
        case .color(let color):
            appearance.configureWithDefaultBackground()
            appearance.backgroundColor = color
        case .image(let image):
            appearance.configureWithDefaultBackground()
            appearance.backgroundImage = image
        default:
            appearance.configureWithDefaultBackground()
        }

        standardAppearance = appearance
        scrollEdgeAppearance = appearance
        compactAppearance = appearance
    }
}

// MARK: - Appearance for iOS 12-
private extension UINavigationBar {

    func setTitleAppearanceForiOS12AndLower(tintColor: UIColor?, font: UIFont?, largeFont: UIFont?) {
        let regularTextAttributes = getTextAttributes(tintColor: tintColor, font: font)
        titleTextAttributes = regularTextAttributes
        // NOTE: On iOS 12 and lower, the font needs to be set globally to get effect on all back bar buttons,
        // so if you ever wanted to use custom font on only one navigation bar and not other,
        // this property needs to be rewritten with each switch between these two navigation bars.
        UIBarButtonItem.appearance().setTitleTextAttributes(regularTextAttributes, for: .normal)
        largeTitleTextAttributes =
            getTextAttributes(tintColor: tintColor, font: largeFont ?? font?.withSize(17))
    }

    func setBackgroundForiOS12AndLower(background: BackgroundType?) {
        backgroundColor = nil
        setBackgroundImage(nil, for: .default)
        shadowImage = nil
        guard let background = background else { return }
        switch background {
        case .transparent:
            setBackgroundImage(getBackgroundImage(color: .white, alpha: 0),
                               for: .default)
            shadowImage = UIImage()
        case .semiTransparent(let color, let alpha, let includeShadow):
            // NOTE: When using semitransparent background image
            // the large title navigation bar remains fully transparent.
            setBackgroundImage(getBackgroundImage(color: color, alpha: alpha),
                               for: .default)
            if !includeShadow {
                shadowImage = UIImage()
            }
        case .color(let color):
            setBackgroundImage(getBackgroundImage(color: color, alpha: 1),
                               for: .default)
        case .image(let image):
            setBackgroundImage(image, for: .default)
        }
    }
}

// MARK: - Supporting methods
private extension UINavigationBar {

    func getTextAttributes(tintColor: UIColor?, font: UIFont?) -> [NSAttributedString.Key: Any] {
        var attributes: [NSAttributedString.Key: Any] = [:]
        if let tintColor = tintColor {
            attributes[NSAttributedString.Key.foregroundColor] = tintColor
        }
        if let font = font {
            attributes[NSAttributedString.Key.font] = font
        }
        return attributes
    }

    func getBackgroundImage(color: UIColor?, alpha: CGFloat) -> UIImage? {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let size = CGSize(width: UIScreen.main.bounds.width,
                          height: frame.size.height + statusBarHeight)
        let backgroundImage = UIGraphicsImageRenderer(size: size).image { context in
            color?.withAlphaComponent(alpha).setFill()
            context.fill(CGRect(origin: .zero, size: size))
        }
        return backgroundImage
    }
}
#endif
