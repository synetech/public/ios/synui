//
//  UIBarButtonItem.swift
//  
//
//  Created by Petr Dušek on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

public extension UIBarButtonItem {

    static func createEmpty() -> UIBarButtonItem {
        return UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }

    @discardableResult
    func setProperty(_ property: BarButtonItemProperties) -> UIBarButtonItem {
        switch property {
        case .title(let title):
            self.title = title
        case .target(let target):
            self.target = target
        case .action(let action):
            self.action = action
        case .color(let color):
            self.tintColor = color
        case .image(let image):
            self.image = image
        }
        return self
    }
}
#endif
