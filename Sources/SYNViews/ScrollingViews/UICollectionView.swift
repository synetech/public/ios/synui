//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 01/11/2019.
//

#if canImport(UIKit)
import UIKit

// MARK: - UICollectionViewCell
public extension UICollectionView {
    
    func dequeueCell<T: UICollectionViewCell>(cellType: T.Type,
                                              for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.identifier,
                                   for: indexPath) as? T ?? T.init()
    }

    func register<T: UICollectionViewCell>(_ cellType: T.Type) {
        register(cellType.self, forCellWithReuseIdentifier: T.identifier)
    }
}

// MARK: - UICollectionReusableView
public extension UICollectionView {
    
    func dequeueHeader<T: UICollectionReusableView>(cellType: T.Type,
                                                    for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                withReuseIdentifier: T.identifier,
                                                for: indexPath) as? T ?? T.init()
    }
    
    func dequeueFooter<T: UICollectionReusableView>(cellType: T.Type,
                                                    for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                withReuseIdentifier: T.identifier,
                                                for: indexPath) as? T ?? T.init()
    }
    
    func registerHeader<T: UICollectionReusableView>(_ cellType: T.Type) {
        register(T.self,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                 withReuseIdentifier: T.identifier)
    }
    
    func registerFooter<T: UICollectionReusableView>(_ cellType: T.Type) {
        register(T.self,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                 withReuseIdentifier: T.identifier)
    }
}
#endif
