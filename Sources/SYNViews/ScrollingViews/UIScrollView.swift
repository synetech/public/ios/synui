//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 01/11/2019.
//

#if canImport(UIKit)
import UIKit

public extension UIScrollView {

    func scrollToTop() {
        guard !isHidden else { return }
        scrollRectToVisible(.top, animated: true)
    }
}

fileprivate extension CGRect {
    
    static var top: CGRect {
        return CGRect(x: 0, y: 0, width: 1, height: 1)
    }
}
#endif
