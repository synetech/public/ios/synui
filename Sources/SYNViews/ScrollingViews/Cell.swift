//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 01/11/2019.
//

#if canImport(UIKit)
import UIKit

public extension UICollectionReusableView {
    
    static var identifier: String {
        return String(describing: type(of: Self.self))
    }
}

public extension UITableViewCell {

    static var identifier: String {
        return String(describing: type(of: Self.self))
    }
}

public extension UITableViewHeaderFooterView {
    
    static var identifier: String {
        return String(describing: type(of: Self.self))
    }
}
#endif
