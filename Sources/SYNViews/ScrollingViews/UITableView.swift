//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 01/11/2019.
//

#if canImport(UIKit)
import UIKit

// MARK: - UITableViewCell
public extension UITableView {
    
    func dequeueCell<T: UITableViewCell>(cellType: T.Type,
                                         for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.identifier,
                                   for: indexPath) as? T ?? T.init()
    }
    
    func register<T: UITableViewCell>(_ cellType: T.Type) {
        register(T.self, forCellReuseIdentifier: T.identifier)
    }
}

// MARK: - UITableViewHeaderFooterView
public extension UITableView {

    func dequeueHeaderFooterView<T: UITableViewHeaderFooterView>(type: T.Type) -> T {
        return dequeueReusableHeaderFooterView(withIdentifier: T.identifier) as? T ?? T.init()
    }

    func register<T: UITableViewHeaderFooterView>(_ cellType: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.identifier)
    }
}
#endif
