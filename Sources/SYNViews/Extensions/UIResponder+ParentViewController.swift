//
//  UIResponder+ParentViewController.swift
//  JetYou
//
//  Created by Lukáš Růžička on 07.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIResponder {

    var parentViewController: UIViewController? {
        next as? UIViewController ?? next?.parentViewController
    }
}

#endif
