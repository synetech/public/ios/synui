//
//  SwiftUIWrapperView.swift
//  JetYou
//
//  Created by Lukáš Růžička on 01.06.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct SwiftUIWrapperView<Content> where Content: View {

    // MARK: - Properties
    private let content: Content

    // MARK: - Init
    public init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
}

// MARK: - View representable
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
extension SwiftUIWrapperView: UIViewControllerRepresentable {

    public func makeUIViewController(context: Context) -> UIHostingController<Content> {
        let hostingVC = UIHostingController(rootView: content)
        hostingVC.view.backgroundColor = .clear
        return hostingVC
    }

    public func updateUIViewController(_ uiViewController: UIHostingController<Content>, context: Context) {}
}
#endif
