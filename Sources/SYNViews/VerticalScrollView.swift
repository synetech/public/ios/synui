//
//  File.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct ScrollViewOffset: PreferenceKey {

    public static var defaultValue: CGFloat = .zero
    public static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {}
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
struct ScrollControlViews: PreferenceKey {

    enum ControlViewType {

        case stable
        case moving
    }

    static var defaultValue: [ControlViewType: CGFloat] = [:]
    static func reduce(value: inout [ControlViewType: CGFloat], nextValue: () -> [ControlViewType: CGFloat]) {
        nextValue().forEach { key, val in
            value[key] = val
        }
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
/// Basic scroll view which provides its `y` offset through `ScrollViewOffset` preference key.
public struct VerticalScrollView<Content>: View where Content: View {

    // MARK: - Properties
    let showsIndicators: Bool
    let content: Content

    @State private var offset: CGFloat = .zero

    // MARK: - Body
    public var body: some View {
        ScrollView(showsIndicators: showsIndicators) {
            GeometryReader { proxy in
                Color.clear
                    .preference(key: ScrollControlViews.self,
                                value: [.moving: proxy.frame(in: .global).minY])
            }
            .frame(height: 0)
            content
                .padding(.top, -8)
        }
        .background(
            GeometryReader { proxy in
                Color.clear
                    .preference(key: ScrollControlViews.self,
                                value: [.stable: proxy.frame(in: .global).minY])
            }
        )
        .onPreferenceChange(ScrollControlViews.self) { value in
            self.offset = (value[.stable] ?? 0) - (value[.moving] ?? 0)
        }
        .preference(key: ScrollViewOffset.self, value: offset)
    }

    // MARK: - Init
    public init(showsIndicators: Bool = true,
         @ViewBuilder content: () -> Content) {
        self.showsIndicators = showsIndicators
        self.content = content()
    }
}

// MARK: - Preview
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
struct VerticalScrollView_Previews: PreviewProvider {

    private struct Preview: View {

        @State private var offset: CGFloat = .zero

        var body: some View {
            VStack {
                Text(String(format: "%1.f", offset))
                    .animation(.default)
                VerticalScrollView(showsIndicators: false) {
                    ForEach(0..<100) {
                        Text("\($0)")
                    }
                }
            }
            .onPreferenceChange(ScrollViewOffset.self, perform: { self.offset = $0 })
        }
    }

    static var previews: some View {
        Preview()
    }
}

#endif
