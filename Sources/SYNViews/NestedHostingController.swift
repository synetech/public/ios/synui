//
//  NestedHostingController.swift
//  JetYou
//
//  Created by Lukáš Růžička on 07.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public class NestedHostingController<T: View>: UIHostingController<T> {

    // MARK: - Lifecycle
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        navigationController?.navigationBar.isHidden = true
    }
}

#endif
