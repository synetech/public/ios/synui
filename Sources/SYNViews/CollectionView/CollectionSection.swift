//
//  CollectionSection.swift
//  JetYou
//
//  Created by Lukáš Růžička on 06.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)

@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
public struct CollectionSection<Section: Hashable, Item: Hashable>: Hashable {

    // MARK: - Properties
    let section: Section
    let items: [Item]

    // MARK: - Init
    public init(section: Section, items: [Item]) {
        self.section = section
        self.items = items
    }
}

#endif
