//
//  HostingCollectionViewCell.swift
//  JetYou
//
//  Created by Lukáš Růžička on 06.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//
// NOTE: - This is inspired by solution from Noah Gilmore
// [https://noahgilmore.com/blog/swiftui-self-sizing-cells/].
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
class HostingCollectionViewCell<Cell, Model>: UICollectionViewCell where Cell: View, Model: Hashable {

    // MARK: - Properties
    private lazy var hostingController: UIHostingController<Cell?> = {
        let controller = NestedHostingController<Cell?>(rootView: nil)
        controller.view.backgroundColor = .clear
        return controller
    }()

    var model: Model?
}

// MARK: - Setup
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
extension HostingCollectionViewCell {

    func setup(with view: Cell, model: Model, parentViewController: UIViewController?) {
        hostingController.rootView = view
        hostingController.view.invalidateIntrinsicContentSize()
        self.model = model

        let requiresControllerMove = hostingController.parent != parentViewController
        if requiresControllerMove {
            removeHostingControllerFromSuperview()
            hostingController.willMove(toParent: parentViewController)
            parentViewController?.addChild(hostingController)
        }

        if !contentView.subviews.contains(hostingController.view) {
            contentView.addSubview(hostingController.view)
            hostingController.view.translatesAutoresizingMaskIntoConstraints = false
            hostingController.view.fill(contentView)
        }

        if requiresControllerMove {
            hostingController.didMove(toParent: parentViewController)
        }
    }

    private func removeHostingControllerFromSuperview() {
        guard contentView.subviews.contains(hostingController.view) else { return }
        hostingController.willMove(toParent: nil)
        hostingController.view.removeFromSuperview()
        hostingController.removeFromParent()
        hostingController.didMove(toParent: nil)
    }
}

private extension UIView {

    func fill(_ view: UIView) {
        topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    }
}

#endif
