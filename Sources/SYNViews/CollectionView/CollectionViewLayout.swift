//
//  CollectionViewLayout.swift
//  JetYou
//
//  Created by Lukáš Růžička on 06.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import UIKit

@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
public class CollectionViewLayout {

    // MARK: - Aliases
    public typealias Layout = (Int, NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection

    // MARK: - Subtypes
    public enum ItemHeight {

        case dynamic(estimatedHeight: CGFloat = 40)
        case fixed(_ height: CGFloat)
    }
}

// MARK: - Predefined layouts
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
public extension CollectionViewLayout {

    static func list(itemHeight: ItemHeight = .dynamic(),
                     itemSpacing: CGFloat = 0,
                     sectionTopPadding: CGFloat = 0,
                     sectionBottomPadding: CGFloat = 0) -> Layout {
        { _, layoutEnvironment in
            let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: itemHeight.heightDimension)
            let item = NSCollectionLayoutItem(layoutSize: size)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: size, subitem: item, count: 1)

            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: sectionTopPadding,
                                                            leading: 0,
                                                            bottom: sectionBottomPadding,
                                                            trailing: 0)
            section.interGroupSpacing = itemSpacing
            return section
        }
    }
}

// MARK: - Height dimensions
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
extension CollectionViewLayout.ItemHeight {

    var heightDimension: NSCollectionLayoutDimension {
        switch self {
        case .dynamic(let estimatedHeight):
            return .estimated(estimatedHeight)
        case .fixed(let height):
            return .absolute(height)
        }
    }
}
#endif
