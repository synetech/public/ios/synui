//
//  CollectionView.swift
//  JetYou
//
//  Created by Lukáš Růžička on 06.05.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//
// NOTE: - This is inspired by solution from Samuel Défago
// [https://defagos.github.io/swiftui_collection_part2/].
//

#if canImport(UIKit)
import Combine
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct CollectionView<Section: Hashable, Item: Hashable, Cell: View> {

    // MARK: - Aliases
    public typealias SectionLayoutProvider = (Int, NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection
    typealias HostCell = HostingCollectionViewCell<Cell, Item>

    // MARK: - Subtypes
    public class Coordinator: NSObject, UICollectionViewDelegate {

        typealias DataSource = UICollectionViewDiffableDataSource<Section, Item>

        var parent: CollectionView
        var dataSource: DataSource?

        var currentSectionsHash: Int?

        var highlightedCells: [IndexPath] = []

        var cancellables: [AnyCancellable] = []

        init(_ parent: CollectionView) {
            self.parent = parent
        }

        // MARK: - Collection view delegate
        @objc
        public func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
            guard parent.updateCellOnHighlight else { return }
            highlightedCells.append(indexPath)
            reloadRow(at: indexPath, collectionView: collectionView)
        }

        @objc
        public func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
            guard parent.updateCellOnHighlight else { return }
            highlightedCells.removeAll(where: { indexPath == $0 })
            reloadRow(at: indexPath, collectionView: collectionView)
        }
    }

    /// Model with all needed parameters for building the cell.
    public struct CellBuilderModel {

        public let indexPath: IndexPath
        /// Indicates whether the cell should be shown as highlighted. In order to make this work properly,
        /// you need to pass `true` to `updateCellOnHighlight` parameter.
        public let isHighlighted: Bool
        public let data: Item
    }

    // MARK: - Properties
    private let sections: [CollectionSection<Section, Item>]
    private let sectionLayoutProvider: SectionLayoutProvider

    private let showScrollIndicator: Bool

    private let shouldScrollToTop: Bool
    private let onScrollOffsetChanged: ((CGPoint) -> Void)?
    private let onScrolledToEnd: (() -> Void)?

    private let updateCellOnHighlight: Bool

    private let cellBuilder: (CellBuilderModel) -> Cell

    // MARK: - Init
    public init(sections: [CollectionSection<Section, Item>],
                sectionLayoutProvider: @escaping SectionLayoutProvider,
                showScrollIndicator: Bool = false,
                shouldScrollToTop: Bool = false,
                onScrollOffsetChanged: ((CGPoint) -> Void)? = nil,
                onScrolledToEnd: (() -> Void)? = nil,
                updateCellOnHighlight: Bool = false,
                @ViewBuilder cellBuilder: @escaping (CellBuilderModel) -> Cell) {
        self.sections = sections
        self.sectionLayoutProvider = sectionLayoutProvider
        self.showScrollIndicator = showScrollIndicator
        self.shouldScrollToTop = shouldScrollToTop
        self.onScrollOffsetChanged = onScrollOffsetChanged
        self.onScrolledToEnd = onScrolledToEnd
        self.updateCellOnHighlight = updateCellOnHighlight
        self.cellBuilder = cellBuilder
    }
}

// MARK: - View representable
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
extension CollectionView: UIViewControllerRepresentable {

    public func makeUIViewController(context: Context) -> UICollectionViewController {
        let collectionController = UICollectionViewController(collectionViewLayout: getLayout(context: context))
        collectionController.collectionView.backgroundColor = .clear
        collectionController.collectionView.showsVerticalScrollIndicator = showScrollIndicator
        collectionController.collectionView.clipsToBounds = false
        collectionController.collectionView.delegate = context.coordinator
        collectionController.collectionView.register(HostCell.self)
        setup(with: collectionController.collectionView, context: context)
        return collectionController
    }

    public func updateUIViewController(_ collectionController: UICollectionViewController, context: Context) {
        reloadData(coordinator: context.coordinator, animated: true)
        if shouldScrollToTop {
            collectionController.collectionView.setContentOffset(.zero, animated: true)
        }
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
}

// MARK: - Setup
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, *)
private extension CollectionView {

    func getLayout(context: Context) -> UICollectionViewLayout {
        UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            context.coordinator.parent.sectionLayoutProvider(sectionIndex, layoutEnvironment)
        }
    }

    func setup(with collectionView: UICollectionView, context: Context) {
        context.coordinator.dataSource =
            Coordinator.DataSource(collectionView: collectionView) { collectionView, indexPath, item in
                let hostCell = collectionView.dequeueCell(cellType: HostCell.self, for: indexPath)
                hostCell.setup(with: cellBuilder(.init(indexPath: indexPath,
                                                       isHighlighted: context.coordinator.isHighlighted(indexPath),
                                                       data: item)),
                               model: item, parentViewController: collectionView.parentViewController)
                return hostCell
            }
        reloadData(coordinator: context.coordinator)
        context.coordinator.bindScrollOffset(in: collectionView)
    }

    func reloadData(coordinator: Coordinator, animated: Bool = false) {
        guard let dataSource = coordinator.dataSource else { return }
        let sectionsHash = sections.hashValue
        guard coordinator.currentSectionsHash != sectionsHash else { return }
        dataSource.apply(getDataSnapshot(), animatingDifferences: true)
        coordinator.currentSectionsHash = sectionsHash
    }

    func getDataSnapshot() -> NSDiffableDataSourceSnapshot<Section, Item> {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Item>()
        for row in sections {
            snapshot.appendSections([row.section])
            snapshot.appendItems(row.items, toSection: row.section)
        }
        return snapshot
    }
}

// MARK: - Data binding
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension CollectionView.Coordinator {

    func bindScrollOffset(in collectionView: UICollectionView) {
        collectionView.publisher(for: \.contentOffset)
            .sink(receiveValue: { [weak self, weak collectionView] contentOffset in
                guard let self = self, let collectionView = collectionView else { return }
                self.parent.onScrollOffsetChanged?(contentOffset)
                let endOffset = collectionView.contentSize.height - collectionView.frame.height * 1.5
                if endOffset > 0 && contentOffset.y >= endOffset {
                    self.parent.onScrolledToEnd?()
                }
            })
            .store(in: &cancellables)
    }
}

// MARK: - Supporting functions
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension CollectionView.Coordinator {

    func isHighlighted(_ indexPath: IndexPath) -> Bool {
        highlightedCells.contains(indexPath)
    }

    func reloadRow(at indexPath: IndexPath, collectionView: UICollectionView) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CollectionView.HostCell,
              let model = cell.model else { return }
        cell.setup(with: parent.cellBuilder(.init(indexPath: indexPath,
                                                  isHighlighted: isHighlighted(indexPath),
                                                  data: model)),
                   model: model, parentViewController: collectionView.parentViewController)
    }
}

#if canImport(SwiftUI)
// MARK: - Preview
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
struct CollectionView_Previews: PreviewProvider {

    typealias Section = CollectionSection<Int, String>

    static var sections: [Section] = {
        var sections = [Section]()
        for i in 0..<10 {
            sections.append(Section(section: i, items: (0..<10).map { "\(i),\($0)" }))
        }
        return sections
    }()

    static var previews: some View {
        CollectionView(sections: sections,
                       sectionLayoutProvider: CollectionViewLayout.list(
                        itemSpacing: 8,
                        sectionTopPadding: 24,
                        sectionBottomPadding: 80)) { model in
            Text("model.data") // Fixes issue in Xcode 14.3 that fails to resolove model.data
                .padding()
                .background(Color.yellow)
                .cornerRadius(8)
        }
    }
}
#endif
#endif
