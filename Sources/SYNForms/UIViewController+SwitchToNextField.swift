//
//  UIViewController+SwitchToNextField.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIViewController {

    /// Changes focus on next field in order when user taps on `Enter` in keyboard.
    /// - Parameters:
    ///   - orderedFields: All fields ordered as how they're shown on screen.
    ///   - currentField: Currently active field.
    func switchToNextField(orderedFields: [UITextField], currentField: UITextField) {
        guard let index = orderedFields.firstIndex(of: currentField) else { return }
        let followingIndex = index + 1
        if followingIndex < orderedFields.count {
            orderedFields[followingIndex].becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
    }
}
#endif
