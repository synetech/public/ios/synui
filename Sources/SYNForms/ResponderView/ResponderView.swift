//
//  ResponderView.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct ResponderView {

    // MARK: - Properties
    let tag: Int
    let onBecomeResponder: () -> Void

    // MARK: - Init
    public init(tag: Int, onBecomeResponder: @escaping () -> Void) {
        self.tag = tag
        self.onBecomeResponder = onBecomeResponder
    }
}

// MARK: - View representable
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
extension ResponderView: UIViewRepresentable {

    public func makeUIView(context: Context) -> some UIView {
        UIResponderView(tag: tag, onBecomeResponder: onBecomeResponder)
    }

    public func updateUIView(_ uiView: UIViewType, context: Context) {}
}

// MARK: - Preview
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
struct ResponderView_Previews: PreviewProvider {

    static var previews: some View {
        ResponderView(tag: 0, onBecomeResponder: {})
            .previewLayout(.sizeThatFits)
    }
}

#endif
