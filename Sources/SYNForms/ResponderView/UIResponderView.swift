//
//  File.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public class UIResponderView: UIView {

    // MARK: - Properties
    private let onBecomeResponder: () -> Void

    // MARK: - Init
    public init(tag: Int, onBecomeResponder: @escaping () -> Void) {
        self.onBecomeResponder = onBecomeResponder
        super.init(frame: .zero)

        self.tag = tag
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Interactions
extension UIResponderView {

    func triggerBecomeResponderEvent() {
        onBecomeResponder()
    }
}

#endif
