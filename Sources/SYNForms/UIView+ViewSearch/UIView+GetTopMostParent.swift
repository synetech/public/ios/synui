//
//  UIView+GetTopMostParent.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    func getTopMostParent() -> UIView? {
        guard superview != nil else { return nil }
        if superview?.superview != nil {
            return superview?.getTopMostParent()
        } else {
            return superview
        }
    }
}

#endif
