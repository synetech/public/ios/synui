//
//  UIView+FindViewsWithTagInHierarchy.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    func findViewsInHierarchy(withTag tag: Int) -> [UIView] {
        var views: [UIView] = []
        subviews.forEach {
            if $0.tag == tag {
                views.append($0)
            } else {
                views.append(contentsOf: $0.findViewsInHierarchy(withTag: tag))
            }
        }
        return views
    }
}

#endif
