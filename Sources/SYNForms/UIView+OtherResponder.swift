//
//  UIView+OtherResponder.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    var doesPreviousResponderExists: Bool {
        getPreviousResponder() != nil
    }

    var doesNextResponderExists: Bool {
        getNextResponder() != nil
    }

    func switchToPreviousResponder() {
        if let previousResponder = getPreviousResponder() {
            handleOncomingResponder(previousResponder)
        } else {
            resignFirstResponder()
        }
    }

    func switchToNextResponder() {
        if let nextResponder = getNextResponder() {
            handleOncomingResponder(nextResponder)
        } else {
            resignFirstResponder()
        }
    }
}

// MARK: - Supporting functions
private extension UIView {

    func getPreviousResponder() -> UIView? {
        getTopMostParent()?.findViewsInHierarchy(withTag: tag - 1).first
    }

    func getNextResponder() -> UIView? {
        getTopMostParent()?.findViewsInHierarchy(withTag: tag + 1).first
    }

    func handleOncomingResponder(_ oncomingResponder: UIView) {
        if oncomingResponder.canBecomeFirstResponder {
            oncomingResponder.becomeFirstResponder()
        } else {
            resignFirstResponder()
            (oncomingResponder as? UIResponderView)?.triggerBecomeResponderEvent()
        }
    }
}

#endif
