//
//  UIViewController.swift
//  
//
//  Created by Vojtěch Pajer on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

public extension UIViewController {
    
    /// Gives view controllers the capability to show custom created `AlertViewController`s.
    ///
    ///  Suggested usage:
    ///  1. Create `AlertAction` enum that conforms to `AlertActionEnum` protocol. See example in `AlertActionEnum` class documentation.
    ///  2. Create `AlertControllerStyle` enum that conforms to `AlertControllerStyleEnum` protocol. See example in `AlertControllerStyleEnum` class documentation. 
    ///  3. For each `UIAlertController` in your app create a case in `AlertControllerStyle`.
    ///  4. Invoke the `UIAlertController` with the following code:
    ///
    ///      ```
    ///      let alertControllerStyle = AlertControllerStyle.example(
    ///          okAction: { [weak self] _ in self?.router.onOkTapped(),
    ///          cancelAction: { [weak self] _ in self?.router.onCancelTapped() }
    ///      })
    ///      presentAlertViewController(withStyle: alertControllerStyle)
    ///      ```
    ///
    ///  5. Enjoy the beauty of your code without duplicate `UIAlertController` creation.
    ///
    func presentAlertViewController(withStyle style: AlertControllerStyle, tintColor: UIColor? = nil) {
        let alertController = UIAlertController(title: style.title,
                                                message: style.message,
                                                preferredStyle: style.preferredStyle)
        alertController.add(actions: style.actions)
        alertController.setPopoverController()
        present(alertController, animated: true, completion: nil)
        if let tintColor = tintColor {
            alertController.view.tintColor = tintColor
        }
    }
}
#endif
