//
//  AlertControllerStyle.swift
//
//
//  Created by Vojtěch Pajer on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

/// - Note: Suggested usage:
/// ```
/// enum MyAlertControllerStyle: AlertControllerStyle {
///
///    case example(okAction: ActionHandler, cancelAction: ActionHandler)
///
///    var preferredStyle: UIAlertController.Style {
///        switch self {
///        case .example:
///            return .alert
///        }
///    }
///
///    var title: String? {
///        switch self {
///        case .example:
///            return R.string.localizable.exampleTitle()
///        }
///    }
///
///    var message: String? {
///        switch self {
///        case .example:
///            return R.string.localizable.exampleMessage()
///        }
///    }
///
///    var actions: [AlertAction] {
///        switch self {
///        case .example(let okAction, let cancelAction):
///            return [
///                .ok(action: okAction)
///                .cancelWithAction(action: cancelAction)
///            ]
///        }
///    }
/// }
/// ```
public protocol AlertControllerStyle {
    
    typealias ActionHandler = (UIAlertAction) -> Void
    
    var preferredStyle: UIAlertController.Style { get }
    
    var title: String? { get }
    var message: String? { get }
    
    var actions: [AlertActionEnum] { get }
}

@available(*, unavailable, renamed: "AlertControllerStyle")
typealias AlertControllerStyleEnum = AlertControllerStyle
#endif
