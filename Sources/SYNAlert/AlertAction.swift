//
//  AlertAction.swift
//
//
//  Created by Vojtěch Pajer on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

/// - Note: Suggested usage:
/// ```
/// enum AlertAction: AlertActionEnum {
///
///     case action(title: String, action: ActionHandler)
///     case ok(action: ActionHandler)
///     case cancel
///     case cancelWithAction(action: ActionHandler)
///     case destructive(title: String, action: ActionHandler)
///
///     var action: UIAlertAction {
///         switch self {
///         case .action(let title, let action):
///             return UIAlertAction(title: title, style: .default, handler: action)
///         case .ok(let action):
///             return UIAlertAction(title: R.string.localizable.lbl_ok(), style: .default, handler: action)
///         case .cancel:
///             return UIAlertAction(title: R.string.localizable.lbl_cancel(), style: .cancel, handler: nil)
///         case .cancelWithAction(let action):
///             return UIAlertAction(title: R.string.localizable.lbl_cancel(), style: .cancel, handler: action)
///         case .destructive(let title, let action):
///             return UIAlertAction(title: title, style: .destructive, handler: action)
///         }
///     }
/// }
/// ```

public protocol AlertActionEnum {
    
    typealias ActionHandler = ((UIAlertAction) -> Void)?
    
    var action: UIAlertAction { get }
}
#endif
