//
//  UIAlertController.swift
//  
//
//  Created by Vojtěch Pajer on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

public extension UIAlertController {
    
    func add(actions: [AlertActionEnum]) {
        actions.forEach { addAction($0.action) }
    }
    
    func setPopoverController() {
        if let popoverController = popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: view.bounds.midX,
                                                  y: view.bounds.maxY,
                                                  width: 0, height: 0)
        }
    }
    
    func setPopoverController(sourceView: UIView) {
        if let popoverController = popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = CGRect(x: sourceView.bounds.midX,
                                                  y: sourceView.bounds.maxY,
                                                  width: 0, height: 0)
        }
    }
}
#endif
