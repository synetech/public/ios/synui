//
//  VectorImage.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct VectorImage {

    // MARK: - Properties
    private let name: String
    private let contentMode: UIView.ContentMode?
    private let tintColor: UIColor?

    // MARK: - Init
    public init(name: String, contentMode: UIView.ContentMode? = nil, tintColor: UIColor? = nil) {
        self.name = name
        self.contentMode = contentMode
        self.tintColor = tintColor
    }
}

// MARK: - View representable
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
extension VectorImage: UIViewRepresentable {

    public func makeUIView(context: Context) -> UIImageView {
        let img = UIImageView()
        img.setContentCompressionResistancePriority(.fittingSizeLevel, for: .vertical)
        return img
    }

    public func updateUIView(_ img: UIImageView, context: Context) {
        if let contentMode = contentMode {
            img.contentMode = contentMode
        }
        if let tintColor = tintColor {
            img.tintColor = tintColor
        }
        img.image = UIImage(named: name)
    }
}

#endif
