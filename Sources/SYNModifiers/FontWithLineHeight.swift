//
//  FontWithLineHeight.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(UIKit)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    func fontWithLineHeight(font: UIFont, lineHeight: CGFloat) -> some View {
        modifier(FontWithLineHeight(font: font, lineHeight: lineHeight))
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private struct FontWithLineHeight: ViewModifier {

    // MARK: - Properties
    let font: UIFont
    let lineHeight: CGFloat

    // MARK: - Body
    @ViewBuilder
    func body(content: Content) -> some View {
        let linePadding: CGFloat = abs(lineHeight - font.lineHeight) / 2
        content
            .font(Font(font))
            .lineSpacing(linePadding * 2)
            .padding(.vertical, linePadding)
    }
}

#endif
