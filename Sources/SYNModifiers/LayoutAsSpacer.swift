//
//  LayoutAsSpacer.swift
//  JetYou
//
//  Created by Lukáš Růžička on 02.06.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    func layoutAsSpacer(alignment: LayoutAsSpacer.Alignment = .center) -> some View {
        modifier(LayoutAsSpacer(alignment: alignment))
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct LayoutAsSpacer: ViewModifier {

    // MARK: - Subtypes
    public enum Alignment {

        case leading, trailing, center
    }

    // MARK: - Properties
    let alignment: Alignment

    // MARK: - Body
    public func body(content: Content) -> some View {
        Spacer()
            .overlay(
                HStack {
                    if alignment == .trailing {
                        Spacer()
                    }
                    content
                    if alignment == .leading {
                        Spacer()
                    }
                }
            )
    }
}
#endif
