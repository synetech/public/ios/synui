//
//  View+CircleFrame.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    func circleFrame(size: CGFloat) -> some View {
        return frame(size: size)
            .cornerRadius(size / 2)
    }
}

#endif
