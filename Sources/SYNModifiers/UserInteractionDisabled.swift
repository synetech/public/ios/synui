//
//  UserInteractionDisabled.swift
//  JetYou
//
//  Created by Lukáš Růžička on 01.06.2021.
//  Copyright © 2021 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI
import SYNViews

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    func disableUserInteraction() -> some View {
        modifier(UserInteractionDisabled())
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private struct UserInteractionDisabled: ViewModifier {

    func body(content: Content) -> some View {
        SwiftUIWrapperView {
            content
        }
        .allowsHitTesting(false)
    }
}
#endif
