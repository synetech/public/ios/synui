//
//  ScaleToFitWidth.swift
//  
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    /// Simulates behaviour of `adjustsFontSizeToFitWidth` from UIKit. In advance, this could be used on any view, not only text/label.
    /// - Parameter minimumScale: What should be the minimal scale.
    /// - Returns: Modified view.
    func scaleToFitWidth(minimumScale: CGFloat = 0.8) -> some View {
        modifier(ScaleToFitWidth(minimumScale: minimumScale))
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private struct ScaleToFitWidth: ViewModifier {

    // MARK: - Properties
    let minimumScale: CGFloat

    // MARK: - Body
    func body(content: Content) -> some View {
        content
            .lineLimit(1)
            .minimumScaleFactor(minimumScale)
    }
}

#endif
