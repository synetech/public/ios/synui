//
//  AdvancedShadow.swift
//
//
//  Created by Lukáš Růžička on 23.04.2021.
//

#if canImport(SwiftUI)
import SwiftUI
import SYNModifiers

//swiftlint:disable identifier_name
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public extension View {

    /// Applies custom shadow implementation with advanced (not offered natively) options
    /// like `spread` or optimized off-screen rendering using Metal framework.
    /// - Parameters:
    ///   - shape: There are currently 3 predefined shapes: `rectangle` with corner radius,
    ///   `capsule` (rendered as circle if its frame is rect)
    ///   and custom `path` which has to be passed along with its stroke width.
    ///   - color: Color of the shadow.
    ///   Can already have opacity applied - then you don't need to use `opacity` parameter.
    ///   - opacity: Alpha value of the shadow color. Use interval `0-1`.
    ///   - blur: Blur effect value.
    ///   - spread: Points of which the shadow should be larger than its view.
    ///   - x: Offset of the shadow on the x axis.
    ///   - y: Offset of the shadow on the y axis.
    ///   - useMetalRendering: Whether the shadow should use optimized off-screen rendering using Metal framework.
    ///   It causes longer initial loading but it doesn't slow down the rendering performance afterwards.
    ///   It can also cause higher usage of the RAM. Default is `true`.
    /// - Returns: Modified `View`.
    func advancedShadow(shape: AdvancedShadow.ShadowShape,
                        color: Color, opacity: Double = 1,
                        blur: CGFloat = 0, spread: CGFloat = 0,
                        x: CGFloat = 0, y: CGFloat = 0,
                        useMetalRendering: Bool = true) -> some View {
        modifier(AdvancedShadow(shape: shape, color: color, opacity: opacity,
                                blur: blur, spread: spread, x: x, y: y,
                                useMetalRendering: useMetalRendering))
    }
}

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
public struct AdvancedShadow: ViewModifier {

    // MARK: - Subtypes
    public enum ShadowShape {

        case rectangle(cornerRadius: CGFloat)
        /// Rendered as circle if `width == height`
        case capsule
        case path(_ path: Path, lineWidth: CGFloat)
    }

    // MARK: - Properties
    let shape: ShadowShape
    let color: Color
    let opacity: Double
    let blur: CGFloat
    let spread: CGFloat
    let x: CGFloat
    let y: CGFloat
    let useMetalRendering: Bool

    // MARK: - Body
    public func body(content: Content) -> some View {
        content
            .background(
                GeometryReader { proxy in
                    shape
                        .getShapeView(color: color, spread: spread, geometryProxy: proxy)
                        .opacity(opacity)
                        .blur(radius: blur)
                        .modify(if: useMetalRendering, {
                            $0
                                .padding(blur)
                                .drawingGroup()
                                .padding(-blur)
                        })
                }
                // NOTE: - The `GeometryReader` content can't expand to leading or top direction
                .padding([.top, .leading], -spread)
                .offset(x: x, y: y)
            )
    }
}

// MARK: - Shadow shape views
@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private extension AdvancedShadow.ShadowShape {

    @ViewBuilder
    func getShapeView(color: Color, spread: CGFloat, geometryProxy: GeometryProxy) -> some View {
        let shadowWidth = geometryProxy.size.width + spread
        let shadowHeight = geometryProxy.size.height + spread
        switch self {
        case .rectangle(let cornerRadius):
            RoundedRectangle(cornerRadius: cornerRadius)
                .fill(color)
                .frame(width: shadowWidth, height: shadowHeight)
        case .capsule:
            Capsule()
                .fill(color)
                .frame(width: shadowWidth, height: shadowHeight)
        case .path(let path, let width):
            path
                .applying(CGAffineTransform(scaleX: shadowWidth / geometryProxy.size.width,
                                            y: shadowHeight / geometryProxy.size.height)
                )
                .stroke(color, lineWidth: width + spread)
                .frame(width: shadowWidth + width, height: shadowWidth + width)
                .padding([.top, .leading], spread / 2)
        }
    }
}

#endif
