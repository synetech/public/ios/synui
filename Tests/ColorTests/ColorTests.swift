import XCTest
@testable import SYNColor

final class ColorTests: XCTestCase {
    
    #if canImport(UIKit)
    let testAlphaValue: Int = 128
    let testWhiteColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    let testBrownColor = UIColor(red: 170 / 255.0, green: 121 / 255.0, blue: 66 / 255.0, alpha: 1.0)
    var testBrownColorWithAlpha: UIColor {
        testBrownColor.withAlphaComponent(128 / 255.0)
    }
    
    func testIntValueInit() {
        let brownColor = UIColor(red: 170, green: 121, blue: 66)
        let brownColorWithAlpha = UIColor(red: 170, green: 121, blue: 66, alpha: testAlphaValue)
        let whiteColor = UIColor(red: 255, green: 255, blue: 255)
        
        XCTAssertEqual(testBrownColor, brownColor)
        XCTAssertEqual(testBrownColorWithAlpha, brownColorWithAlpha)
        XCTAssertEqual(testWhiteColor, whiteColor)
    }
    
    func testIntHexInit() {
        let brownColor = UIColor(hex: 0xAA7942)
        let brownColorWithAlpha = UIColor(hexWithAlpha: 0xAA794280)
        let whiteColor = UIColor(hex: 0xFFFFFF)
        
        XCTAssertEqual(testBrownColor, brownColor)
        XCTAssertEqual(testBrownColorWithAlpha, brownColorWithAlpha)
        XCTAssertEqual(testWhiteColor, whiteColor)
    }
    
    func testStringHexInit() {
        let brownColor = UIColor(hex: "#AA7942")
        let brownColorWithAlpha = UIColor(hexWithAlpha: "#AA794280")
        let whiteColor = UIColor(hex: "#FFFFFF")
        let wrongHexColor = UIColor(hex: "#GGAA77CC")
        let shortHexColor = UIColor(hex: "#AABBCCD")
        let noHexColor = UIColor(hex: "AA794280")
        
        
        XCTAssertEqual(testBrownColor, brownColor)
        XCTAssertEqual(testBrownColorWithAlpha, brownColorWithAlpha)
        XCTAssertEqual(testWhiteColor, whiteColor)
        XCTAssertEqual(wrongHexColor, nil)
        XCTAssertEqual(shortHexColor, nil)
        XCTAssertEqual(noHexColor, nil)
    }
    #endif
}
