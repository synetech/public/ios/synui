## 1.1.2 Release notes (5-10-2023)

### Bugfixes
- `R.swift` conflicting naming with native asset generation fixed

## 1.1.0 Release notes (30-11-2022)

### Breaking Changes
- `R.swift` version raised to 7.0.1 - this change includes change of the package source!


## 1.0.0 Release notes (11-11-2022)

### Breaking Changes
- `Tutorial` target removed
- `Layout` target moved to SYNAncient library
- Removed `Stevia` dependency
- `Base` target moved to SYNAncient library
- Removed `RxSwift` dependency
- Added `SYN` prefix to all remaining targets
- Minimal version raised to iOS 13

## 0.1.4 Release notes (29-08-2022)

### Bugfixes
- Changed Stevia repo.
- Changed Rswift repo.

## 0.1.3 Release notes (09-01-2022)

### Enhancements
- `onTap` extension for `UIView` added.

## 0.1.2 Release notes (7-10-2021)

### Breaking Changes
- `AlertControllerStyleEnum` renamed to `AlertControllerStyle`.

### Enhancements
- Added dequeuing of table view header/footer.
- Added extension for adjusting width/height based on already assigned height/width and the image ratio.
- Added extension for dynamic changes of table footer height.
- Added automatic switching of focus to following text field when user taps `Enter`.
- RxSwift raised to 6.x.
- Added `SYNImage` target with `VectorImage` SwiftUI view which makes using vector assets possible.
- Added `SYNRSwift` target with extension on all Rswift resource types for creating SwiftUI views.
- Added option to pass tint color of alert.
- Added `SYNModifiers` target with some useful SwiftUI modifiers.
- Added `SYNShadow` target with `AdvancedShadow` SwiftUI modifier.
- Added `SYNViews` target with `VerticalScrollView` which provides scroll offset.
- Added option to switch form fields by tags.
- Added SwiftUI wrapper for `UICollectionView`.
- `CollectionView` enhanced with option to recreate cells when they're highlighted.
- Added simple wrapper view which wraps SwiftUI view to UIKit view.
- Added SwiftUI modifier which ensures that SwiftUI view doesn't block interaction of UIKit view under it.
- Added SwiftUI modifier which aligns the view in stack as a `Spacer` (fills available space).
- Added option to show scroll indicator on `CollectionView`.

### Bugfixes
- `setPopoverController` function made public.
- Fixed public identifier for cells, because of crashing.


## 0.1.1 Release notes (09-11-2020)

### Enhancements
- Added functionality for setting `UINavigationBar` appearance.
- Added function for filling whole safe area with one call.
- Added function for adjusting height/width of view regardless of whether the constraint was assigned before.
- Added function for adding multiple arranged subviews to `UIStackView` at once.


## 0.1.0 Release notes (26-08-2020)

### Breaking Changes
- UIBarButtonItem empty property changed to function

### Enhancements
- Added Base classes


## 0.0.5 Release notes (08-07-2020)

### Bugfixes
- Added explicit dependency of Stevia for Layout target.


## 0.0.4 Release notes (08-07-2020)

### Enhancements
- Added anchoring to safe area.


## 0.0.3 Release notes (07-01-2019)

### Bugfixes

* Udpated package structure and added more products.


## 0.0.2 Release notes (28-10-2019)

### Enhancements

* Added `Alert` target.
* Added `Color` target.
* Added `ScrollingViews` target.
* Added `Layout` target.
* Added `NavigationBar` target.
* Added `Tutorial` target.

## 0.0.1 Release notes (28-10-2019)

### Breaking Changes
- Init.

## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
