# SYNUI

Pack of useful UI extensions, tricks and other stuffs to do your UI things better.



## Alert

Protocols and extensions to simplify the `UIAlertController` usage. For custom iPad usage consult _@VojtechPajer_ or _@PetrDusek_.



## Color
Set of convenience `UIColor` initializers. 

*Added initializers:*
- `init(red: Int, green: Int, blue: Int, alpha: Int = 255)`
- `init(rgb: Int)`
- `init(rgbWithAlpha: Int)`
- `init(hex: String)`
- `init(hexWithAlpha: String)`



## Forms

Useful extension for forms handling:
- automatically switching focus on following text field when user taps `Enter`

### Switching forms focus by tag
If you set tag for your text fields, you can switch between them by using `switchToPreviousResponder` or `switchToNextResponder` function.

SwiftUI views can't be handled this way, so if you want to make them use tags and react on the `becomeFirstResponder` place `ResponderView` next to them.



## Image
Extensions and tools related to images and image views.

### VectorImage
SwiftUI `Image` view for vector images (SwiftUI ignores vector data by default).



## Modifiers
Provides useful SwiftUI modifiers: 
- conditional modifier - `modify(if:..)`
- simulation of UIKit's `adjustsFontSizeToFitWidth` - `scaleToFitWidth()` 
- setting of line height properly (respects spacing for one line text) - `fontWithLineHeight(font: _, lineHeight: _)`
- shortcut to square frame size - `frame(size: _)`
- shortcut to circle frame - `circleFrame(size: _)`
- cast to any view - `asAnyView()`
- ensures that SwiftUI view doesn't block interaction of UIKit view under it - `disableUserInteraction()`
- aligns the view in stack as a `Spacer` (fills available space) - `layoutAsSpacer`



## Navigation Bar

### UIBarButtonItem
Extension for `UIBarButtonItem` allowing convenient setting of its properties (image, target, ...).

### Appearance
Provides functionality to set navigation bar appearance (with support of iOS 12 and lower, where the setting is much different). You can set `tintColor`, `font`, `largeFont` (for large titles) and `background`, which you can choose from 4 options:
- transparent
- semi-transparent (with setting of color, alpha and whether the bar should include shadow)
- specific color
- image



## RSwift
Extension on all Rswift resource types for creating SwiftUI views.



## Shadow 
Provides `AdvancedShadow` modifier [SwiftUI], which offers more capabalities than the standard SwiftUI modifier. For more info, see [this](https://www.notion.so/synetech/Shadows-e0574c69ad314964b4bc2197aad0d26c).



## Views
Provides useful SwiftUI views.

### CollectionView
SwiftUI wrapper of the `UICollectionView`, which allows you to define the cells as SwiftUI Views. 
It provides option to define its layout using the modern approach, available from iOS 13.0 (for more info, see [the documentation](https://developer.apple.com/documentation/uikit/views_and_controls/collection_views/implementing_modern_collection_views)). There are also few predefined layout options located in `CollectionViewLayout` as static functions.

Usage (with one of the predefined layouts):
```swift
let sections: [] = [
    .init(section: "Houses", items: ["iOS", "QA", "Sales"]),
    .init(section: "Teams", items: ["Oriflame", "JetYou", "ISIC"])
]
CollectionView(sections: sections,
               sectionLayoutProvider: CollectionViewLayout.list()) { _, _, item in
    Text(item)
        .padding()
    Divider()
}
```

### Scrolling Views

`UITableView` and `UICollectionView` extensions simplifying the usage of register and dequeue functions. 
Fuction for scrolling to the top of the `scrollView`. 

### VerticalScrollView
Basic scroll view which provides its `y` offset through `ScrollViewOffset` preference key.

Usage:
```swift
VerticalScrollView(showsIndicators: false) {
    Text("Scroll view content here")
}
.onPreferenceChange(ScrollViewOffset.self, perform: { print("Scroll offset is: \($0)") })
```

### NestedHostingController
`UIHostingController` which ensures that the nested view won't affect navigation bar visibility of the parent `UINavigationController`. If you use the standard one in nested views, it automatically reveals the navigation bar.

### SwiftUIWrapperView
Just simple `UIViewRepresentable` view which wraps SwiftUI view to UIKit view.
